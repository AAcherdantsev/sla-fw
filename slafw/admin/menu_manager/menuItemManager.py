import re
import json
from typing import Dict

from slafw.admin.items import AdminItem

JSON_DEFAUL_KEY = "default"
JSON_ITEMS_KEY = "items"
JSON_VISIBLE_KEY = "visible"
JSON_DEFAUL_VALUE = True
MENU_ITEM_MANAGER_CONFIG_FILE_PATH = "config_menu_item_manager.json"
ARE_ITEMS_DEFAUL_VISIBLE = True

class ItemStateData:
    def __init__(self):
        self.visible: Dict[str, bool] = {}

    def is_item_visible(self, printer_name: str) -> bool:
        if printer_name in self.visible:
            return self.visible[printer_name]
        else:
            return self.get_default_visible()

    def set_default_visible(self) -> None:
        self.visible[JSON_DEFAUL_KEY] = JSON_DEFAUL_VALUE

    def get_default_visible(self) -> bool:
        return self.visible[JSON_DEFAUL_KEY]

    def to_dict(self):
        return {
            JSON_VISIBLE_KEY: self.visible
        }

    @classmethod
    def from_dict(cls, data: dict):
        item_state_data = cls()
        item_state_data.visible = data[JSON_VISIBLE_KEY]
        return item_state_data


class MenuData:
    def __init__(self):
        self.items: Dict[str, ItemStateData] = {}

    def is_item_visible(self, printer_name: str, item_identifier: str):
        element = self.items.get(item_identifier)

        if element is None:
            return ARE_ITEMS_DEFAUL_VISIBLE

        return element.is_item_visible(printer_name)

    def to_dict(self):
        return {
            JSON_ITEMS_KEY: {key: value.to_dict() for key, value in self.items.items()}
        }

    @classmethod
    def from_dict(cls, data: dict):
        menu_data = cls()
        menu_data.items = {key: ItemStateData.from_dict(value) for key, value in data[JSON_ITEMS_KEY].items()}
        return menu_data


class MenuItemsManager:
    printer_name = ""
    menus: Dict[str, MenuData] = {}

    @staticmethod
    def setup(printer_name: str):
        MenuItemsManager.printer_name = printer_name
        MenuItemsManager.load_from_file()

    @staticmethod
    def load_from_file(file_name: str = MENU_ITEM_MANAGER_CONFIG_FILE_PATH):
        with open(file_name, 'r') as file:
            data = json.load(file)
            MenuItemsManager.menus = {key: MenuData.from_dict(value) for key, value in data.items()}

    @staticmethod
    def generate_config_file(file_name: str):
        with open(file_name, 'w') as file:
            json.dump(MenuItemsManager.menus, file, default=lambda x: x.to_dict(), indent=4)

    @staticmethod
    def get_item_name(item: AdminItem):
        # TODO: restrict identifier being None typed
        if (item.identifier is not None):
            return item.identifier
        
        if (item.name is not None):
            return MenuItemsManager.convert_name_to_id(item.name)
            
        return None

    @staticmethod
    def register_item(menu_obj, item: AdminItem):
        menu_identifier = menu_obj.__class__.__name__
        item_identifier = MenuItemsManager.get_item_name(item)
        
        # if it was not possible to get item ID, ignore the element
        if (item_identifier is None): 
            return

        menu = MenuItemsManager.menus.get(menu_identifier)

        if menu is None:
            menu = MenuData()
            MenuItemsManager.menus[menu_identifier] = menu

        if menu.items.get(item_identifier) is not None:
            return # ignoring duplicates
                   # TODO: restrict duplicates, his may be achieved
                   # after making "identidier" field mandatory

        new_item = ItemStateData()
        new_item.set_default_visible()

        menu.items[item_identifier] = new_item

    @staticmethod
    def is_item_visible(menu_obj, item: AdminItem):
        menu_identifier = menu_obj.__class__.__name__
        item_identifier = MenuItemsManager.get_item_name(item)

        menu = MenuItemsManager.menus.get(menu_identifier)

        if menu is None:
            return ARE_ITEMS_DEFAUL_VISIBLE
        
        return menu.is_item_visible(MenuItemsManager.printer_name, item_identifier)

    @staticmethod
    def convert_name_to_id(name) -> str:
        # Removing HTML tags
        result = re.sub(r'<[^>]+>', '', name)
        
        # Remove brackets, dashes and commas
        result = re.sub(r'[(),\-]', '', result)
        
        # Remove the spaces and replace them with an underscore, convert to lower
        result = re.sub(r'\s+', '_', result.strip()).lower()

        return result
