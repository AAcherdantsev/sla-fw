from abc import ABC
from datetime import datetime

from slafw.admin.manager import AdminManager
from slafw.admin.safe_menu import SafeAdminMenu

from slafw.admin.menus.root import RootMenu
from slafw.admin.menus.feature_m1.root import OnlyM1FeatureMenu

from slafw.admin.menus.settings.root import SettingsRoot
from slafw.admin.menus.settings.base import SettingsMenu
from slafw.admin.menus.settings.backup import BackupConfigMenu

from slafw.admin.menus.hardware.root import HardwareRoot
from slafw.admin.menus.hardware.display import ExposureDisplayMenu
from slafw.admin.menus.hardware.profiles import EditProfiles

from slafw.admin.menus.firmware.root import FirmwareRoot
from slafw.admin.menus.firmware.net_update import NetUpdate, FwInstall
from slafw.admin.menus.firmware.system_info import SystemInfoMenu
from slafw.admin.menus.firmware.wizards_test import WizardsTestMenu
from slafw.admin.menus.firmware.admin_api_test import ApiTestMenu, TestMenu2

from slafw.configs.hw import HwConfig
from slafw.hardware.printer_model import PrinterModelVirtual
from slafw.admin.menu_manager.menuItemManager import MenuItemsManager

from slafw.admin.menus.hardware.tests import (
    AxisTimingTest, 
    HardwareTestMenu, 
    InfiniteTestMenu, 
    ResinSensorTestMenu, 
    ExposureProfilesMenu,
    InfiniteUVCalibratorMenu
)

# mock:

class IPrinter(ABC):
    pass


class TiltMock():
    def __init__(self) -> None:
        self.profiles = []
        pass


class HardwareSL1Mock():
    def __init__(self) -> None:
        self.axes = {}
        self.printer_model = PrinterModelVirtual()
        self.fans = {}
        self.cpuSerialNo = "SERIAL_NUMBER"
        self.system_version = "SYSTEM_VERSION"
        self.config = HwConfig()
        self.tilt = TiltMock()
        self.tower = TiltMock()

    def get_writer(self) -> None:
        pass


class InetMock():
    def download_url(self, *args, **kwargs):
        pass


class PrinterMock(IPrinter):
    def __init__(self) -> None:
        self.hw = HardwareSL1Mock()
        self.inet = InetMock()


def mock_runner(self):
    pass


def generate_json():
    menus = []
    printer = PrinterMock()  
    admin_control = AdminManager()

    NetUpdate._runner = mock_runner
    ApiTestMenu._runner = mock_runner
    InfiniteTestMenu._runner_init = mock_runner
    InfiniteTestMenu._runner_tilt = mock_runner    
    InfiniteTestMenu._runner_tower = mock_runner
    ResinSensorTestMenu._runner = mock_runner
    InfiniteUVCalibratorMenu._runner = mock_runner

    menus.append(TestMenu2(admin_control))
    menus.append(ApiTestMenu(admin_control))
    menus.append(SafeAdminMenu(admin_control))
    menus.append(OnlyM1FeatureMenu(admin_control))
    menus.append(InfiniteUVCalibratorMenu(admin_control))

    menus.append(RootMenu(admin_control, printer))
    menus.append(NetUpdate(admin_control, printer))
    menus.append(HardwareRoot(admin_control, printer))
    menus.append(FirmwareRoot(admin_control, printer))
    menus.append(SettingsRoot(admin_control, printer))
    menus.append(SettingsMenu(admin_control, printer))
    menus.append(SystemInfoMenu(admin_control, printer))
    menus.append(AxisTimingTest(admin_control, printer))
    menus.append(WizardsTestMenu(admin_control, printer))
    menus.append(HardwareTestMenu(admin_control, printer))
    menus.append(BackupConfigMenu(admin_control, printer))
    menus.append(ResinSensorTestMenu(admin_control, printer))
    menus.append(ExposureDisplayMenu(admin_control, printer))
    menus.append(ExposureProfilesMenu(admin_control, printer))

    menus.append(EditProfiles(admin_control, printer, pset=[]))
    menus.append(FwInstall(admin_control, printer, firmware={'version': 'v1.2.3'}))

    for menu in menus: 
        for item in menu.items.values():
            MenuItemsManager.register_item(menu, item)

    time = datetime.now().strftime("%Y.%m.%d_%H:%M:%S")
    MenuItemsManager.generate_config_file(f"config_menu_item_{time}.json")

generate_json()