from slafw.admin.menu import AdminMenu
from slafw.admin.items import AdminLabel
from slafw.admin.control import AdminControl

class OnlyM1FeatureMenu(AdminMenu):
    def __init__(self, control: AdminControl):
        super().__init__(control)

        self.add_back()
        self.add_item(AdminLabel("Hello to the M1 printer", identifier="only_M1_label"))